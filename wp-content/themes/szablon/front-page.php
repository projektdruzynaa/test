<?php 
	/*
	Template Name: Front-page
	*/
 ?>
<?php get_header(); ?>

	<div class="photo">
		<div class="content fade-in-down">
			<div class="container">
				<div class="col-md-4 col-md-offset-4">
					<?php get_product_search_form(); ?>
				</div>
			</div>
		</div>
		<img class="mainphoto" src="<?php echo IMAGES?>/firstpage.jpg" alt="Szkolenia">
	</div> <!-- end photo -->


	<div class="info-boxes"> <!-- start info-boxes -->
		<div class="container">
			<div class="row fade-in-up">
				<div class="col-md-4">
					<div class="info-box">
						<h2>Oferta</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur.
						</p>
						<a href="#" class="button read-more">Więcej <i class="fa fa-chevron-right"></i> </a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info-box">
						<h2>Doświadczenie</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur.
						</p>
						<a href="#" class="button read-more">Więcej <i class="fa fa-chevron-right"></i> </a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info-box">
						<h2>Jakość</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur.
						</p>
						<a href="#" class="button read-more">Więcej <i class="fa fa-chevron-right"></i> </a>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- end info-boxes -->

 <?php get_footer(); ?>