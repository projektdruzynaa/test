<?php 
	define('THEMEROOT', get_stylesheet_directory_uri()); //ODNOSI SIE DO CALEGO KATALOGU Z SZABLONEM
	define('IMAGES', THEMEROOT . '/images' ); // kropka działa jak + w js
	define('STYLES', THEMEROOT . '/styles');
	define('SCRIPTS', THEMEROOT . '/scripts');

	//styles
	function add_styles (){
		wp_enqueue_style('bootstrap', STYLES . "/bootstrap.min.css");
		wp_enqueue_style('font-awesome', STYLES . "/font-awesome.css");
		wp_enqueue_style('style', THEMEROOT . "/style.css");
	}

	add_action('wp_enqueue_scripts', 'add_styles'); //dodaje akcje ladowania stylów lub skryptow

	//scripts
	function add_scripts(){
		wp_enqueue_script('custom', SCRIPTS . "/custom.js", array('jquery'), '1.0', false);
	} //ostatni parametr false oznacza ze nie wczytuje sie w stopce tylkow sekcji header

	add_action('wp_enqueue_scripts', 'add_scripts');

	//add menu
	function menu() {
		register_nav_menus(
			array(
				'main-menu' => 'Głowne menu'
				)
			);
	}

	add_action('init', 'menu');

	//funkcja od search

	function add_search( $items, $args) {
		if( $args -> theme_location == 'main-menu' ){
			return $items . '<li class="search">
								<a href="#"><i class="fa fa-search"></i></a>
								<form action="'.get_site_url().'" method="get">
									<input type="text" id="s" name="s" placeholder="Wyszukaj...">
								</form>
							</li>';
		}
		return $items;

	}

	add_filter('wp_nav_menu_items','add_search', 10, 2);


	//add sidebars widgets

	register_sidebar(array(
		'name' => 'Pierwsza kolumna stopki',
		'id'=> 'footer-1',
		'description' => 'Przykladowy opis',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
		'before_widget' => '<div class="footer-box">',
		'after_widget' => '</div>'
		));
	register_sidebar(array(
		'name' => 'Druga kolumna stopki',
		'id'=> 'footer-2',
		'description' => 'Przykladowy opis',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
		'before_widget' => '<div class="footer-box">',
		'after_widget' => '</div>'
		));
	register_sidebar(array(
		'name' => 'Trzecia kolumna stopki',
		'id'=> 'footer-3',
		'description' => 'Przykladowy opis',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
		'before_widget' => '<div class="footer-box">',
		'after_widget' => '</div>'
		));
	register_sidebar(array(
		'name' => 'Sidebar Aktualnosci',
		'id'=> 'index-1',
		'description' => 'Przykladowy opis',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
		'before_widget' => '<div class="block">',
		'after_widget' => '</div>'
		));
	
	register_sidebar(array(
		'name' => 'Menu Startpage',
		'id'=> 'start-page-1',
		'description' => 'Przykladowy opis',
		'before_title' => '<h3 class="log-in-header">',
		'after_title' => '</h3>',
		'before_widget' => '<div class="block-log-in">',
		'after_widget' => '</div>'
		));
	add_theme_support ('post-thumbnails');
?>