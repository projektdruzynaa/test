<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Skrypt wp-config.php używa tego pliku podczas instalacji.
 * Nie musisz dokonywać konfiguracji przy pomocy przeglądarki internetowej,
 * możesz też skopiować ten plik, nazwać kopię "wp-config.php"
 * i wpisać wartości ręcznie.
 *
 * Ten plik zawiera konfigurację:
 *
 * * ustawień MySQL-a,
 * * tajnych kluczy,
 * * prefiksu nazw tabel w bazie danych,
 * * ABSPATH.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', '21841965_projekt');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', '21841965_projekt');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'WaCwpQC5Us01');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'serwer1630158.home.pl');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8mb4');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p2-E~s{`<~3P3{~sS m}Aqd$$_aXD07bEvia]if+#EjNdt(4|CWlFIm.LXzb~#>F');
define('SECURE_AUTH_KEY',  'gk;SNW+-]R[Bv2~S3b^wl[[#tW^Pc|C2;/0xdfF>{q6XM9fz|9p;kOlLoVw. =lb');
define('LOGGED_IN_KEY',    'MIxs>kPmxX<+{9g/QoWko,x|a0}+n>VC/(d<&eU||Y4:K=xH^/YTv3{/<%eVN^n@');
define('NONCE_KEY',        '9^l 3bH!dt&ar=bC!iF*|O?QcUj&_5~TqdFBv-1>7%JB.Q_3mf1$(Y.+g#W[z~Z]');
define('AUTH_SALT',        '(#3d`RNv?`y~.+81pJz9EvomUB#4PSWc}>vxLtx|bD[(;kDbSenS Q#5Ii2<s,PY');
define('SECURE_AUTH_SALT', 'vG1gR*<BIvA8w a+a,V,5g~>W]nSUl1:~ykk0}bi=BF)?nhe<w4v lMv9Yr`?j&c');
define('LOGGED_IN_SALT',   'caGAr&5@Inw(@63`e|>8YJikKP~tt*61~y6]B.+~^Cw}V:=R|s?x*Ply^a&K=~jg');
define('NONCE_SALT',       'u~3:QPkpTh_m|:th)FbGA+DgkcC*opiP*:(;n?HtoPW$[(msbn,`<9iV:$4:D<uj');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie
 * ostrzeżeń podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG podczas pracy nad nimi.
 *
 * Aby uzyskać informacje o innych stałych, które mogą zostać użyte
 * do debugowania, przejdź na stronę Kodeksu WordPressa.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
